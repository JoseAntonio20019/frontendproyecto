<?php  

$nombre=htmlspecialchars(($_POST["nombre"]?? null));
$email=htmlspecialchars(($_POST["correo"]?? null));
$subject=htmlspecialchars(($_POST["subject"]?? null));
$apellido=htmlspecialchars(($_POST["apellido"]?? null));

if ($_SERVER['REQUEST_METHOD'] === 'POST'){

$errores=array();

    if(empty($nombre)){
        
         $errores["nombre"]="Nombre obligatorio, introduzca uno.";
       
    }
    
    if(empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)){

        $errores["email"]="Email inválido o vacío,introduzca uno válido.";

    }

    if(empty($subject)){

         $errores["subject"]="Subject no introducida, introduzca una";
    }

}
    include __DIR__ . "/utils/utils.php";
    require "views/contact.view.php";
    

?>